<?php

require_once("config.php");

ob_start();
echo date("Y-m-d H:i:s") . " Opening file... <br>\n";
$csv_original = file_get_contents($_FILES['csv']['tmp_name']);


echo date("Y-m-d H:i:s") . " Spliting new line <br>\n";
$csv_original_rows = split("\r\n", $csv_original);

// print_r($csv_original_rows); exit;

for($i = 0; $i < count($csv_original_rows); $i++){
    echo date("Y-m-d H:i:s") . " Spliting separator \$i = $i <br>\n";
    $csv_original_rows[$i] = split(_SEPARATOR, $csv_original_rows[$i]);
}

if(count($csv_original_rows) == 0){
    echo "Empty row";
    exit;
}


echo date("Y-m-d H:i:s") . " Spliting separator <br>\n";
$header_count = count($csv_original_rows[0]);
echo date("Y-m-d H:i:s") . " \$header_count = $header_count <br>\n";
$row_to_remove = array();


echo date("Y-m-d H:i:s") . " Begin checking new line every row <br>\n";
for($i = 0; $i < count($csv_original_rows); $i++){

    
    echo date("Y-m-d H:i:s") . " Checking at \$i = $i <br>\n";
    // $column_count = count($csv_original_rows[$i]);

        // Marking how much row to make as one row
    $bottom_to_merge = 0;

    // Checking multiple row to join as one row
    while(count($csv_original_rows[$i]) < $header_count){

        // Marking how much row to make as one row
        $bottom_to_merge++;

        // Check array index to prevent overflow column
        $i_first_row_last_column = count($csv_original_rows[$i]) - 1; 

        // Merge bottom row to upper row
        $csv_original_rows[$i] = array_merge($csv_original_rows[$i], $csv_original_rows[$i + $bottom_to_merge]);

        // Mark bottom row to remove it later
        $row_to_remove[] = $i + $bottom_to_merge;

        // Join duplicate column as one column
        $csv_original_rows[$i][$i_first_row_last_column] = $csv_original_rows[$i][$i_first_row_last_column] . $csv_original_rows[$i][$i_first_row_last_column + 1];

        // Delete after join
        unset($csv_original_rows[$i][$i_first_row_last_column + 1]);
    }

    if($bottom_to_merge != 0){
        $i = $i + $bottom_to_merge;
        echo date("Y-m-d H:i:s") . " Found, \$bottom_to_merge = $bottom_to_merge <br>\n";
    }
}

// Sort array 9 to 1
natsort($row_to_remove);
$row_to_remove = array_reverse($row_to_remove);

foreach($row_to_remove as $one_i){
    unset($csv_original_rows[$one_i]);
}


// echo str_replace("  ", " &nbsp; ", nl2br(print_r($csv_original_rows, true)));
ob_clean();


// Begin export to CSV

// Open the output stream
$fh = fopen('php://output', 'w');

// Start output buffering (to capture stream contents)
ob_start();

// CSV Data
foreach ($csv_original_rows as $row) {
    fputcsv($fh, $row, '|');
}

// Get the contents of the output buffer
$string = trim( ob_get_clean() );
// $string = str_replace('"'."\n".'"', '', $string);

// Output CSV-specific headers
header('Pragma: public');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Cache-Control: private', false);
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename="' . $_POST['filename'] . '.csv";');
header('Content-Transfer-Encoding: binary');

// Stream the CSV data
// exit(preg_replace("/\r\n|\r|\n/", "", $string));
exit($string);

